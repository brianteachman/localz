<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Filter\HtmlEntities;
use TWeb\Model\DirectoryLister;
use TWeb\Model\FileReader;

class IndexController extends AbstractActionController
{
    public $current_dir;

    public $current_path = 'C:\www\localz';
    // public $current_path = 'C:\www';
    
    protected $ent;
    protected $dirtool;
    protected $parser;

    public function __construct() {
        $this->ent = new HtmlEntities();
        $this->dirtool = new DirectoryLister();
        $this->parser = new FileReader($this->ent);
        $this->current_dir = $this->dirtool->arrayizeDirectory($this->current_path);

    }

    public function indexAction()
    {
        $this->layout('layout/f-layout');

        if (strstr($_SERVER['OS'], 'Windows')) {
            $vhost_list = $this->dirtool->readyWindowsVhosts();
        } else {
            // todo
            // $vhost_path = '/etc/apache2/sites-enabled';
        }

        if ($this->params()->fromQuery('i')) {

            $this->current_path = $this->params()->fromQuery('i');

            if (is_dir($this->current_path)) {
                $this->current_dir = $this->dirtool->arrayizeDirectory($this->current_path);
            } elseif (is_file($this->current_path)) {

                return $this->forward()->dispatch('Application\Controller\Index', array(
                    'action' => 'listing'
                ));
            }
        }
        
        // $this->killView($vhost_list);
        // $this->killView($this->current_dir);

        return new ViewModel(array(
            'current_path' => $this->current_path,
            'slug' => $this->current_path . DIRECTORY_SEPARATOR,
            'files' => $this->current_dir,
            'vhosts' => $vhost_list,
        ));
    }

    public function listingAction() {
        
        $content = $this->params()->fromQuery('i', $this->current_path);

        $fparts = pathinfo($content);
        if (is_dir($fparts['dirname'])) {
            $this->current_path = $fparts['dirname'];
            $this->current_dir = $this->dirtool->arrayizeDirectory($this->current_path);
        }

        $view_params = [
            'content' => $content,
            'current_path' => $this->current_path,
            'file_name' => $fparts['basename'],
            'dir_name' => $fparts['dirname'] . DIRECTORY_SEPARATOR,
            'files' => $this->current_dir,
        ];
        
        if (is_file($content)) {

            $view_params['content'] = $this->parser->parse($content);
            $view_params['file_type'] = $fparts['extension'];

        }

        return new ViewModel($view_params);
    }

    public function editAction() {
        // 
    }

    public function killView($var_to_view) {
        echo "<pre>";
        print_r($var_to_view);
        echo "</pre>"; die;
    }
}
