<?php
/**
 * 
 * @package Library
 * 
 */
namespace Library\Controller;

use Library\Model\BookShelf;
use Library\Model\Book;
use Library\Model\GoodReadBook;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Json\Json;

/**
 * @package Library
 */
class LibraryController extends AbstractActionController
{
	public $library;

    public function __construct()
    {
        $this->library = new BookShelf();
    }

    /**
     * Index - Library
     * 
     * @return Zend\View\Model\ViewModel Zend View Model
     */
    public function indexAction()
    {
    	return new ViewModel(array('books' => $this->library->books));
    }

    public function bookAction()
    {
    	if ($this->params('isbn')) {

            $book_data = $this->library->books[$this->params('isbn')];

            // $book = new GoodReadBook($book_data);
            $book = new Book($book_data);

            return new ViewModel(
                array('book' => $book)
            );
    	}
        
    }

    public function buildCoverArray($books) {
        // print('<pre>');
        // print_r($books->info); 
        // print_r($book->info->publishers[0]->name); 
        // print_r($book->info->identifiers->isbn_13[0]); 
        // print('</pre>'); die;

        $csv = '';

        foreach ($books->info as $book) {
            if (array_key_exists('identifiers', $book)) {
                // die('Argh...');
                if (isset($book->identifiers->isbn_13[0])) {
                    $csv .= $book->identifiers->isbn_13[0];
                } else {
                    $csv .= $book->identifiers->isbn_10[0];
                }
                
            }
            if (array_key_exists('cover', $book)) {
                $csv .= ', ' . $book->cover->small
                     . ', ' . $book->cover->medium
                     . ', ' . $book->cover->large . "\n";
            }
        }
        echo $csv;
        // file_put_contents('C:\www\localz\data\book_covers.csv', $csv);
    }

}