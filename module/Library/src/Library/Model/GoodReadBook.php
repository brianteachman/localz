<?php
namespace Library\Model;

use Zend\Json\Json;

/**
* 
*/
class GoodReadBook
{
    protected $api_url = "";

    protected $key = "Ua1B5jMJswVQgtNNA";
    protected $secret = "WcKf9C1b2ls9Eq4zMZ8s3MM85MRuJ0Kq0L6IGVbeU";

    public $target;

    public $data;

    public $isbn;
    public $title;
    public $author_details; // Last, First name
    public $publisher;
    public $date_published; // YYYY-MM-DD
    public $pages;
    public $description;


    function __construct($data)
    {
        $this->data = $data;
        $this->isbn = $data['isbn'];
        $this->title = $data['title'];
        $this->author_details = $data['author_details'];
        $this->publisher = $data['publisher'];
        $this->date_published = $data['date_published'];
        $this->pages = $data['pages'];
        $this->description = $data['description'];

    	// $this->target = sprintf($this->api_url, $isbn);
        // $this->data = $this->getJsonVars($this->target);
    }

    public function getJsonVars($target)
    {
        $data = Json::decode(file_get_contents($target));
        return get_object_vars($data);
	}
}