<?php
namespace Library\Model;

use Zend\Json\Json;

/**
* 
*/
class Book extends \ArrayObject
{
    public $isbn;
    public $title;
    public $author;
    public $publisher;

    function __construct($book_data)
    {
        $this->isbn = $book_data["isbn"];
        $this->title = $book_data["title"];
    	$this->author = $book_data["author_details"];
        $this->publisher = $book_data["publisher"];
    }
}