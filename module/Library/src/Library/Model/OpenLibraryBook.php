<?php
namespace Library\Model;

use Zend\Json\Json;

/**
* 
*/
class OpenLibraryBook extends \ArrayObject
{
    protected $googleapis_url = "https://www.googleapis.com/books/v1/volumes?q=isbn:%s";

    protected $openlibrary_url = "http://openlibrary.org/api/books?bibkeys=ISBN:%s&jscmd=data&format=json";

    public $target;

    public $info;

    public $isbn;
    public $title;
    public $author;
    public $publisher;

    function __construct($isbn)
    {
        $this->isbn = $isbn;
        // $this->target = sprintf($this->googleapis_url, $isbn);
    	$this->target = sprintf($this->openlibrary_url, $isbn);

        $this->info = $this->fetchApiData($this->target);
    }

    public function fetchApiData($target)
    {
        $book_data = Json::decode(file_get_contents($target));
        $d = get_object_vars($book_data);
        $book = $d['ISBN:'.$this->isbn];
        return $book;
	}
}