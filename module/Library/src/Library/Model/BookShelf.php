<?php
namespace Library\Model;

/**
 * API links 
 * - https://openlibrary.org/dev/docs/api/books
 * - https://www.goodreads.com/api
 * - https://www.librarything.com/wiki/index.php/LibraryThing_JSON_Books_API
 * 
 * @package Library
 */
class BookShelf extends \ArrayObject
{
	protected $csv_path = 'C:\www\localz\data\export.csv';

    public $books = [];
	
	function __construct()
	{
		$this->books = $this->loadBooksFromCsv($this->csv_path);
	}

	public function loadBooksFromCsv($csv_file) {
        $table_headers = []; // record first row as header (aka array keys)
        $books = [];         // return object  
        $row = 1;
        if (($handle = fopen($csv_file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $book = [];
                $isbn = '';
                $num = count($data);
                for ($c=0; $c < $num; $c++) {
                    if ($row==1) {
                        $table_headers[] = $data[$c];
                    } else {
                        if ($table_headers[$c] === 'isbn') {
                            $isbn = $data[$c];
                        }
                        $book[$table_headers[$c]] = $data[$c];
                    }
                }
                $books[$isbn] = $book;
                $row++;
            }
            fclose($handle);
        }
        return array_filter($books);
    }

}