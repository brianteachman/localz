<?php
/**
 * 
 * @package Library
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Library\Controller\Library' => 'Library\Controller\LibraryController',
        ),
    ),

    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            
            'library' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/library',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Library\Controller',
                        'controller'    => 'Library',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route'    => '/books',
                            'defaults' => array(
                                'action' => 'books',
                            ),
                        ),
                    ),
                    'book' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/book[/:isbn]',
                            'constraints' => array(
                                'isbn' => '[0-9-]*',
                            ),
                            'defaults' => array(
                                'action' => 'book',
                            ),
                        ),
                    ),
                ),
            ),
            // 'library' => array(
            //     'type'    => 'segment',
            //     'options' => array(
            //         'route'    => '/library[/book/:isbn]',
            //         'constraints' => array(
            //             'isbn'     => '[0-9-]+',
            //         ),
            //         'defaults' => array(
            //             'controller' => 'Library\Controller\Library',
            //             'action'     => 'index',
            //             // 'action'     => 'search',
            //             // 'action'     => 'recent',
            //         ),
            //     ),
            // ),

        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'library' => __DIR__ . '/../view',
        ),
    ),
);
?>
