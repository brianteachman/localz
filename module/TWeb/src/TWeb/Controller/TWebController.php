<?php
/**
 * 
 * @package TWeb
 * 
 */
namespace TWeb\Controller;

use TWeb\Model;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 *
 * @package TWeb
 */
class TWebController extends AbstractActionController
{
    /**
     * Index - TWeb
     * 
     * @return Zend\View\Model\ViewModel Zend View Model
     */
    public function indexAction()
    {
       
    }
  
}