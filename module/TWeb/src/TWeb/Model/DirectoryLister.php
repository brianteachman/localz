<?php
/**
 * @link      http://github.com/brianteachman/localdev for the canonical source
 * @copyright Copyright (c) 2013 Brian Teachman
 * @package   Localdev
 * 
 */

namespace TWeb\Model;

/**
 * Localdev
 * 
 */
class DirectoryLister
{
    /**
     * @var array Array of files to ignore.
     */
    private $exceptions = array(
        '000-default',
        'favicon.ico',
    );

    /**
     * Directory listing currently being read
     *
     * @var array Array of associative arrays
     */
    protected $files;

    protected $config = null;

    public function __construct ($config=null)
    {
        $this->files = new \ArrayObject();

        if (null !== $config) {
            $this->config = $config;
        }
    }
    
    // public function __toString() {}

    /**
     * 
     * @param  string  $dir_path Path to list contents of.
     * @return false|array       Array of files[(type, name)] or false.
     */
    public function listFiles($dir_path)
    {
        if ( ! is_dir($dir_path)) {
            return false;
        }
        
        $iter = new \RecursiveDirectoryIterator($dir_path);
        
        foreach (new \RecursiveIteratorIterator($iter) as $item) {
            $file = array();
            if (is_dir($item)) {
                $file['type'] = 'dir';
                $file['name'] = (string)$item;
            } else {
                $file['type'] = 'file';
                $file['name'] = (string)$item;
            }
            if ( ! preg_match('/^\./', $file['name'])) {
                $this->files->append($file);
            }
        }
        $fileCount = count($this->files);
        for ($i=0; $i < $fileCount; $i++) {
            $dir = basename($this->files[$i]['name']);
            if ($dir == '.' || $dir == '..') {
                $this->files[$i] = '';
            }
        }
        
        return array_filter($this->files->getArrayCopy());
    }
    

    /**
     * Directory structure to array
     *
     * See: http://stackoverflow.com/a/3556894
     * 
     * @param  DirectoryIterator|string $directory
     * @return array
     */
    public function arrayizeDirectory($directory) {

        if ( ! $directory instanceof \DirectoryIterator) {
            if (is_string($directory) && is_dir($directory)) {
                $directory = new \DirectoryIterator($directory);
            } else {
                throw new \InvalidArgumentException("Not valid filename or iterator.");
            }
        }

        $files_array = array();
        foreach ($directory as $key => $child) {
            $name = $child->getBasename();

            // no hidden files
            if ($child->isDot() || $name[0] == '.') {
                continue;
            }
            
            if ($child->isDir()) {
                $subit = new \DirectoryIterator($child->getPathname());
                $files_array[$name] = $this->arrayizeDirectory($subit);
            } else {
                $files_array[] = $name;
            }
        }
        return $files_array;
    }

    /**
     * What does this do?
     *
     * @param  string|array   $resource
     * @param  null|array[][] $usr_exceptions
     * @param  bool           $show_hidden
     * @return false|string[] Array of string resource names or false if empty(array)
     */
    public function readyResults($resource, $usr_exceptions=null, $show_hidden=false) {
        $results = false;

        if ( ! @is_readable($resource)) {
            return $results;
        }
        
        if (isset($usr_exceptions) && is_array($usr_exceptions)) {
            $this->exceptions = array_merge($this->exceptions, $usr_exceptions);
        }
        
        if (@is_dir($resource)) {
            $results = true;
            $resource = $this->listFiles($resource);
        }
        if (@is_file($resource)) {
            $results = true;
            //
            // $this->parseDocument($resource);
        }
        if (is_array($resource)) {
            foreach($resource as $result) {
                $result = str_replace('.conf', '', $result);
                if (substr($result, -1) == '~') continue;
                if ($show_hidden === false) {
                    if (preg_match('/^\./', $result)) continue;
                }
                if (!in_array($result, $this->exceptions)) continue;
                
                $results[] = $result;
            }
            if (is_array($results)) {
                sort($results);
            }
        }
        
        return $results;
    }

    /**
     * Read Vhosts file from default ZendServer location
     */
    public function readyWindowsVhosts() {

        $vhost_path = 'C:\Zend\ZendServer\etc\sites.d';
        if ( ! is_dir($vhost_path)) {
            // $vhost_path = 'C:\Wamp\...';
        }
        if ( ! is_dir($vhost_path)) {
            // $vhost_path = 'C:\var\www\...';
        }
        if ( ! is_dir($vhost_path)) {
            throw new \InvalidArgumentException('Cant find vhosts directory!');
        }

        // $vhosts = $this->listFiles($vhost_path);
        $vhosts = $this->arrayizeDirectory($vhost_path);

        $vhost_list = [];

        foreach($vhosts as $key => $target) {
            $vhost = [];
            if (is_array($target)) {
                continue;
            }
            if (strstr($target, 'zend-default')) {
                continue;
            }
            $vhost_name = str_replace('.conf', '', $target);
            $vhost_name = str_replace('vhost_', '', $vhost_name);
            
            $vhost['name'] = $vhost_name;
            $vhost['filename'] = $target;
            $vhost_list[] = $vhost;
        }
        sort($vhost_list);

        // $this->killView($vhost_list);
        return $vhost_list;
    }
}
