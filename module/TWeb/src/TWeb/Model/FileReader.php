<?php
/**
 * 
 */
namespace TWeb\Model;

use dflydev\markdown\MarkdownParser;

/**
* 
*/
class FileReader
{
    protected $escaper;

    public function __construct($escaper) {
        $this->escaper = $escaper;
    }

    /**
     * File handler
     *
     * @param  string     $directory File or directory name
     * @param  null|array $options   [description]
     * @return [type]            [description]
     */
    public function parse($directory, $options=null) {
        $content = '';
        $listing = trim($directory);

        if (strpos($listing, '.md') || strpos($listing, '.markdown')) {

            $markdown = file_get_contents($listing);
            $mdParser = new MarkdownParser();
            $content = $mdParser->transform($markdown);
            
        } elseif (strpos($listing, '.php') || strpos($listing, '.html')) {

            $file = file_get_contents($listing);
            // Zend\Filter\HtmlEntities::filter
            $content = $this->escaper->filter($file);
            
        } else {
            $content = file_get_contents($listing);
        }

        return $content;
    }
}