<?php
/**
 * 
 * @package TWeb
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'TWeb\Controller\TWeb' => 'TWeb\Controller\TWebController',
        ),
    ),

    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'tweb' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/tweb[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'TWeb\Controller\TWeb',
                        'action'     => 'index',
                        'action'     => 'search',
                        'action'     => 'recent',
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'tweb' => __DIR__ . '/../view',
        ),
    ),
);
?>
