# LocalhostUI
> LICENSE: [WTFPL](http://sam.zoy.org/wtfpl/COPYING)

A user interface for basic LAMP development built using:

+ [Zend Framework 2](https://github.com/phly/phlyty)
+ [Foundation CSS Framework](http://foundation.zurb.com/)
+ [SyntaxHighlighter](http://alexgorbatchev.com/SyntaxHighlighter/)

Upon entering your localhost in a web browser this package provides links for quick access to:

+ the web root directory 
+ the VirtualHost directory found on Ubuntu flavored debian
+ phpMyAdmin, for MySQL needs
+ [The HAL Browser](https://github.com/mikekelly/hal-browser)
+ webgrind
+ Server info
+ Local documentation/notes

> This was implemented on a debian build and as such may need to be configured to suit your system, see step 2 below.

Install:

1. Clone localhost-ui into your root web directory.
   + `cd /var/www` (or wherever)
   + `git clone git@bitbucket.org:brianteachman/localz.git`


-------------------------------------------------------------------------------

[![endorse](http://api.coderwall.com/lordbushi/endorsecount.png)](http://coderwall.com/lordbushi)

DONE:

+ <code>7/19/2014</code>: Implement [Syntax highlighter](http://alexgorbatchev.com/SyntaxHighlighter/)

TODO:

* Integrate [text editor](https://github.com/lordbushi/Quite_Simple_PHP_File_Editor)
